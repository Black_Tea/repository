using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;

namespace ООП_ПЗ_1
{
   class Person
   {
      private bool gender = false;

      public string Name { get; }
      public string LastName { get; }
      public string Patronymic { get; }
      public bool Gender => gender;
      public float Height { get; }
      public DateTime BirthDate { get; }
      public int Age
      {
         get
         {
            var today = DateTime.Today;
            var age = today.Year - BirthDate.Year;
            if (BirthDate.Date > today.AddYears(-age))
               age--;
            return age;
         }
      }
      //Конструктор класса
      public Person(string name, string lastName, string patronymic, bool gender, float height, DateTime birthdate)
      {
         Name = name;
         LastName = lastName;
         Patronymic = patronymic;
         this.gender = gender;
         Height = height;
         BirthDate = birthdate;
      }
      public static Person Parse(string text)
      {
         string[] data = text.Split(' ');
         Person person = new Person(data[0], data[1], data[2],
             data[3] == "муж" ? true : false, float.Parse(data[4]), DateTime.Parse(data[5]));
         return person;
      }
      public override string ToString()
      {
         string str = $"| {Name,15} | {LastName,15} | {Patronymic,15} | {(Gender ? "муж" : "жен"),5} | {Height,8:0.0000} м | " +
                     $"{BirthDate.ToString("dd/MM/yyyy", CultureInfo.InvariantCulture),10} | {Age} лет";
         return str;
      }
   }
   class Program
   {
      static void Main(string[] args)
      {
         List<Person> Persons = new List<Person>();
         string[] PersonsStr = File.ReadAllLines("in.txt");
         foreach (string s in PersonsStr) { Persons.Add(Person.Parse(s)); }

         Console.WriteLine("Сортировка по возрастанию - 1\nСортировка по убыванию - 2");
         switch (char.Parse(Console.ReadLine()))
         {
            case '1'://Метод Sort принимает параметр Comparison<Person> описанный лямбда-выражением
               Persons.Sort((x, y) => x.BirthDate.CompareTo(y.BirthDate));
               break;
            case '2':
               Persons.Sort((x, y) => y.BirthDate.CompareTo(x.BirthDate));
               break;
         }

         using (var file = new StreamWriter("out.txt"))
            foreach (var p in Persons)
               file.WriteLine(p.ToString());
      }
   }
}